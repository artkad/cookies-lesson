import { User } from '../domain/user'
import { Cart } from '../domain/cart'
import { totalPrice } from './product'

export type OrderStatus = "new" | "delivery" | "completed";
type OrderProducts = (user: User, cart: Cart) => Promise<void>;

export type Order = {
  user: UniqueId;
  cart: Cart;
  created: DateTimeString;
  status: OrderStatus;
  total: PriceCents;
};

export function createOrder(user: User, cart: Cart): Order {
  return {
    cart,
    user: user.id,
    status: "new",
    created: new Date().toISOString(),
    total: totalPrice(products),
  };
}