import { createOrder } from '../domain/order'
import { User } from '../domain/user'
import { Cart } from '../domain/cart'
const payment: PaymentService = {};
const notifier: NotificationService = {};
const orderStorage: OrdersStorageService = {};

async function orderProducts(user: User, cart: Cart) {
  const order = createOrder(user, cart);

  // Пробуем оплатить заказ;
  // уведомляем пользователя, если что-то не так:
  const paid = await payment.tryPay(order.total);
  if (!paid) return notifier.notify("Оплата не прошла 🤷");

  // Сохраняем результат и очищаем корзину
  const { orders } = orderStorage;
  orderStorage.updateOrders([...orders, order]);
  cartStorage.emptyCart();
}

export function useOrderProducts() {
  const notifier = useNotifier();
  const payment = usePayment();
  const orderStorage = useOrdersStorage();

  async function orderProducts(user: User, cookies: Cookie[]) {
    // …
  }

  return { orderProducts };
}