import { Order } from '../domain/order'
export interface PaymentService {
  tryPay(amount: PriceCents): Promise<boolean>;
}

export interface NotificationService {
  notify(message: string): void;
}

export interface OrdersStorageService {
  orders: Order[];
  updateOrders(orders: Order[]): void;
}