export function Buy() {
  // Получаем доступ к юзкейсу в компоненте:
  const { orderProducts } = useOrderProducts();

  async function handleSubmit(e: React.FormEvent) {
    setLoading(true);
    e.preventDefault();

    // Вызываем функцию юзкейса:
    await orderProducts(user!, cart);
    setLoading(false);
  }

  return (
    <section>
      <h2>Оформить заказ</h2>
      <form onSubmit={handleSubmit}>{/* ... */}</form>
    </section>
  );
}